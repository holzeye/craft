module parser;

import std.stdio;
static import std.file;

import pegged.grammar;
import engine.file;
import engine.tool;
import engine.lang;
import engine.job;
import engine.core;

class NoMatchingItemFoundException : Exception {
    @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null) {
        super(msg, file, line, next);
    }
}

Craft craft;

    string[] parseCraft(ParseTree p) {
        writeln(p);
        craft = new Craft;
 //       CraftFile f = new CraftFile("/Users/Alex/Xcode/craft/src/test.d");
 //       writeln(f.filename);
//        CraftFile[] files = [];
//        foreach(string name; std.file.dirEntries("/Users/Alex/Xcode/craft/src/", "*.{d,cpp}", std.file.SpanMode.breadth)) {
//            CraftFile cf = new CraftFile(name);
//            writeln(cf.filename);
//            files ~= cf;
//        }
//        CraftTool dmd = new CraftTool("DMD", "dmd", ["-od./build"]);
//        CraftLang d = new CraftLang("D", ["d"], dmd, ["-oftest"]);
//        d.getFiles(files);
//        d.execute("o");

        string[] parseToCode(ParseTree p) {
            switch(p.name)
            {
                case "CRAFT":
                    return parseToCode(p.children[0]); // The grammar result has only child: the start rule's parse tree
                case "CRAFT.File":
 //                   string result = "";//\\documentclass{article}\n\\begin{document}\n";
                    foreach(child; p.children) {// child is a ParseTree
                        parseToCode(child);
                    }
                    return []; //~ "\n\\end{document}\n";
                case "CRAFT.Definition":
                    return parseToCode(p.children[0]); // one child only
                case "CRAFT.VariableDefinition":
                    if(p.children[1].name == "CRAFT.BracketCommand") {
                        craft.variables[p.matches[0]] = parseToCode(p.children[1]);
                        //p.matches[1];
                    } else {
                        craft.variables[p.matches[0]] = [p.matches[1]];
                    }
                    writeln(craft.variables[p.matches[0]]);
                    return [p.matches[0]]; // the first match contains the title
                case "CRAFT.BracketCommand":
                    return craft.runFunction(p.matches);/*"\n\\subsection{"
                           ~ p.matches[0] // the first match contains the title
                           ~ "}\n";*/
                //case "Wiki.Emph":
                //    return " \\emph{"
                //           ~ p.matches[0] // the first match contains the text to emphasize
                //           ~ "} ";
                //case "Wiki.List":
                //    string result = "\n\\begin{itemize}\n";
                //    foreach(child; p.children)
                //        result ~= parseToCode(child);
                //    return result ~ "\\end{itemize}\n";
                //case "Wiki.ListElement":
                //    return "\\item " ~ p.matches[0] ~ "\n";
                //case "Wiki.Text":
                //    return p.matches[0];
                case "CRAFT.BracketDefinition":
                    switch(p.matches[0]) {
                        case "lang":
                            write("Found a language: ");
                            CraftLang l = new CraftLang();
                            l.name = p.matches[1];
                            writeln(l.name);
                            foreach(child; p.children) {
                                if(child.name == "CRAFT.BracketStatement") {
                                    foreach(sub; child.children) {
                                        string[] r = parseToCode(sub);
                                        switch(r[0]) {
                                            case "endings":
                                                write("Found an entry for language-specific file endings: ");
                                                l.endings = r[1..r.length];
                                                writeln(l.endings);
                                                break;
                                            case "tool":
                                                writeln("Found a tool entry: " ~ r[1]);
                                                foreach(CraftTool t; craft.tools) {
                                                    if(t.name == r[1]) {
                                                        l.tool = t;  
                                                    }
                                                }
                                                if(l.tool.name == "tool") {
                                                    throw new NoMatchingItemFoundException("There is no such tool as " ~ r[1] ~ ".");
                                                }
                                                break;
                                            case "args":
                                                write("Found an entry for language-specific arguments: ");
                                                writeln(r[1..r.length]);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                            craft.langs ~= l;
                            break;
                        case "tool":
                            write("Found a tool: ");
                            CraftTool t = new CraftTool();
                            t.name = p.matches[1];
                            writeln(t.name);
                            foreach(child; p.children) {
                                if(child.name == "CRAFT.BracketStatement") {
                                    foreach(sub; child.children) {
                                        string[] r = parseToCode(sub);
                                        switch(r[0]) {
                                            case "exec":
                                                write("Found an executable entry: ");
                                                t.exec = child.matches[1];
                                                writeln(t.exec);
                                                break;
                                            case "args":
                                                write("Found an entry for tool-specific arguments: ");
                                                t.args = r[1..r.length];
                                                writeln(t.args);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                            craft.tools ~= t;
                            break;
                        case "job":
                            write("Job detected: ");
                            CraftJob j = new CraftJob();
                            j.name = p.matches[1];
                            writeln(j.name);
                            break;
                        default:
                            writeln("GENERIC KEYWORD.");
                            return ["EMPTY"];
                    }
                case "CRAFT.BracketArg":
                    return p.matches;
                default:
                    return ["EMPTY"];
            }
            return ["EMPTY"];
        }

        return parseToCode(p);
    }

