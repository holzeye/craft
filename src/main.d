module main;

import std.stdio;
static import std.file;
import grammar;
import parser;

int main(string[] args)
{
	string filename;
	string task;

	if(args.length > 1) {
		if(args[1] == "-nyancat") {
			//writeln("mew-mew-mew-mewmew-mew-mewmew-mew-mew-mew-mewmewmewmewmew-mewmewmewmewmewmewmewmew-mew-mew-mewmewmewmewmewmewmewmewmewmewmewmew-mew-mewmewmewmewmewmew-mewmewmew-mew");
			//writeln("mew-mew-mew-mewmew-mew-mewmew-mew-mew-mew-mewmewmewmewmew-mewmewmewmewmewmewmewmew-mew-mew-mewmewmewmewmewmewmewmewmewmewmewmew-mew-mewmewmewmewmewmew-mewmewmew-mew");
			//writeln("mew-mewmewmew-mewmewmewmewmewmewmewmewmewmew-mewmew-mewmewmewmewmewmewmewmewmewmewmewmew-mew-mewmewmew-mewmewmewmewmewmewmewmewmewmew-mew-mewmewmewmewmewmewmewmewmewmew");
			//writeln("mew-mewmewmew-mewmewmewmewmewmewmewmewmewmew-mewmew-mewmewmewmewmewmewmewmewmewmewmewmew-mew-mewmewmew-mewmewmewmewmewmewmewmewmewmew-mew-mewmewmewmewmewmewmewmewmewmew");
			writeln("NYAN CAT!!!");
			return 0;
		}
	}

	switch (args.length) {
		case 1:
			filename = "craft.cfg";
			task = "default";
			break;
		case 2:
			filename = "craft.cfg";
			task = args[1];
			break;
		case 3:
			if(args[1] == "-f") {
				filename = args[2];
			} else {
				throw new Exception("Usage: craft [-f file] [task]");
			}
			task = "default";
			break;
		case 4:
			if(args[1] == "-f") {
				filename = args[2];
			} else {
				throw new Exception("Usage: craft [-f file] [task]");
			}
			task = args[3];
			break;
		default:
			throw new Exception("Usage: craft [-f file] [task]");
	}
	writeln("File: " ~ filename);
	writeln("Task: " ~ task);
	if (!std.file.exists(filename)) {
		writeln("Error: File " ~ filename ~ " does not exist.");
		return 2;
	} else {
		string code = std.file.readText(filename);
		auto result = CRAFT(code);
		parseCraft(result);
	}
	
	return 0;
}
