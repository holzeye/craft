module grammar;

import pegged.grammar;

mixin(grammar(`
CRAFT:
	File <- Definition*

	Definition <
				/ BracketDefinition
				/ VariableDefinition
	BracketDefinition < DefinitionKeyword Identifier (:":" Variable ("as" Identifier)?)? BracketStatement
	VariableDefinition < :"$" Identifier :":" (BracketCommand / VariableContent)

	BracketStatement < :"{" BracketArg+ :"}"
	BracketArg < BracketVariable / BracketCommand / BracketDefinition
#	BracketVariable < BracketKeyword :":" StringLiteral / IntegerLiteral
	BracketVariable <- BracketKeyword :":" :((" " / "\t") *) VariableContent :eol

	BracketCommand < :"{" BracketCommandKeyword ((StringLiteral / UnrestrictedIdentifier) (:"," (StringLiteral / UnrestrictedIdentifier))*)? :"}"

	Variable <- :"$" Identifier
	
	DefinitionKeyword < "job" / "lang" / "task" / "tool"
	BracketKeyword < "args"/ "bin-dir" / "bin-ending" / "endings" / "exec" / "obj-dir" / "obj-ending" / "target" / "tool"
	BracketCommandKeyword < "call" / "find" / "flush" / "type"
	Keyword < "args"/ "as" / "bin-dir" / "bin-ending" / "endings" / "exec" / "find" / "flush" / "job" / "lang" / "obj-dir" / "obj-ending" / "task" / "tool" / "type"
	Identifier <~ !Keyword [_a-zA-Z][_a-zA-Z0-9-]*
	UnrestrictedIdentifier <~ [_a-zA-Z/][_a-zA-Z0-9-/]*

	Spacing <- (blank / Comment)*
	Comment <- BlockComment / LineComment
	BlockComment <~ "/*" (!"*/" .)* "*/"
	LineComment <~ "//" (!eol .)* eol

	StringLiteral <~ :doublequote (!doublequote .)* :doublequote
	IntegerLiteral <- digits
	VariableContent <~ (!eol .)*

`));