module engine.lang;

import std.process;
import std.stdio;

import engine.tool;
import engine.file;

class CraftLang {
	string name = "lang";
	string[] endings = [""];
	CraftTool tool;
	string[] args = [""];
	CraftFile[] files;

	this() {
		
	}

	this(string n, string[] e, CraftTool t, string[] a) {
		name = n;
		endings = e;
		tool = t;
		args = a;
	}

	void getFiles(CraftFile[] sources) {
		for(int i = 0; i < sources.length; i++) {
			CraftFile file = new CraftFile(sources[i].filename);
			foreach(string ending; endings) {
				if(file.ending == ending) {
					files ~= file;
					break;
				}
				destroy(file);
			}
		}
	}

	int execute(string objEnding) {
		string[] shell = tool.shellString ~ args;
		foreach(CraftFile file; files) {
			shell ~= file.filename;
		}
		writeln(shell);
		auto pid = spawnProcess(shell);
		return wait(pid);
	}
}