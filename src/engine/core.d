module engine.core;

import std.stdio;
import std.string;
static import std.file;

import engine.file;
import engine.tool;
import engine.lang;

class Craft {
	string[][string] variables;
	CraftTool[] tools;
	CraftLang[] langs;

	this() {
		
	}

	string[] runFunction(string[] args) {
        switch(args[0]) {
            case "find":
                return find(args[1..args.length]);
            case "call":
                return call(args[1..args.length]);
            default:
                return ["EMPTY"];
        }
	}

	string[] call(string[] args) {
		if(args.length != 1) {
			throw new Exception("There's something wrong with the call() argument count.");
		}
		return ["EMPTY"];
	}

	string[] find(string[] args) {
		if(args.length == 1) {
			throw new Exception("Error: Nothing specified to search for.");
		}
		string endings = "*.{";
		for(int i = 1; i < args.length; i++) {
			foreach(CraftLang l; langs) {
				if(args[i] == l.name) {
					foreach(string s; l.endings) {
						endings ~= s;
						endings ~= ", ";
					}
				}
			}
		}
		if(endings == "*.{") {
			throw new Exception("Error: No valid languages specified for finding.");
		}
		endings ~= "}";
        string[] ret = [];
        foreach(string s; std.file.dirEntries(args[0], endings, std.file.SpanMode.breadth)) {
            ret ~= s;
        }
		return ret;
	}
}