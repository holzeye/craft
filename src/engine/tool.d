module engine.tool;

class CraftTool {
	string name = "tool";
	string exec = "craft";
	string args[] = ["-nyancat"];

	this() {
		
	}

	this(string n, string e, string[] a) {
		name = n;
		exec = e;
		args = a;
	}

	@property string[] shellString() {
		string[] ret = [exec] ~ args;
		return ret;
	}
}