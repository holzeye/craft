module engine.file;

import std.stdio;

class CraftFile {
	string directory = "/habab/";
	string basename = "anan";
	string ending = "a";

	this() {
		
	}

	this(string filename) {
		parseFilename(filename);
	}

	@property void filename(string name) { parseFilename(name); }
	@property string filename() { 
		return directory ~ basename ~ "." ~ ending;
	}
		//writeln(directory);
		//writeln(basename);
		//writeln(ending);

private:
	void parseFilename(string filename) {
		string dir = "";
		string base = "";
		string end = "";
		ulong eob = 0;
		ulong length = filename.length - 1;
		ulong i = length;
		for (; i > 0; i--) {
			if (filename[i] == '.') {
				eob = length - (length - i);
				end = filename[(i + 1)..filename.length];
				break;
			}
		}
		for (; i > 0; i--) {
			if (filename[i] == '/') {
				base = filename[(i + 1)..eob];
				dir = filename[0..(i + 1)];
				break;
			}
		}
		directory = dir;
		basename = base;
		ending = end;
	}
}