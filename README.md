# CRAFT build system

CRAFT aims to be easier to use than other build systems while trying to be as flexible as possible.

**NOTE: CRAFT is still in development stage and will not compile anything.**

## How to build
At the moment the project is build using **make** which may look a little ironic, but most people use make so I also use it:
```bash
cd /path/to/project/
make
```
Note that you will need [DMD](https://github.com/D-Programming-Language/dmd) to compile CRAFT.

## How to use
There is a file called **craft.cfg** included in the repo. Look at it for a language reference; a tutorial is planned.

By default CRAFT will look for a craft.cfg in the directory you run it from. If there is none, it will complain.
If you want to call your build file "foobar.baz", call CRAFT with `-f foobar.baz`.

## With special thanks to...
The [Pegged parser generator](https://github.com/PhilippeSigaud/Pegged) by Phillipe Sigaud is used for parsing.
