# CRAFT Makefile

all: src/main.d src/grammar.d src/parser.d src/engine/*.d
	@dmd -ofcraft -od./build -I./src $^ lib/libpegged.a

clean:
	@rm -rf ./build/* ./craft
